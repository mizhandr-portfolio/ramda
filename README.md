# Ramda

### Tech stack

- HTML/CSS
- Flex
- Grid
- Normalize CSS

### Demo

Live demo is hosted [here](https://mizhandr-portfolio.gitlab.io/ramda)

### Launch

To run the project follow the next steps:

- No magic here - simple HTML

# Preview

![Project preview](/preview.png)
